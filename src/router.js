import { createRouter, createWebHistory } from "vue-router";
import App from './App.vue';

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
/*import RegistroCursosInteres from './components/RegistroCursosInteres.vue'
import RegistroRol from './components/RegistroRol.vue'*/
import HomeLandingP from './components/HomeLandingP.vue'
import HomeUser from './components/HomeUser.vue'
import HomeComoCertificarte from './components/HomeComoCertificarte.vue'
import HomeCursos from './components/HomeCursos.vue'
import HomeQuienesSomos from './components/HomeQuienesSomos.vue'
import Account from './components/Account.vue'
/*import accountMisCertificados from './components/accountMisCertificados.vue'
import accountEditar from './components/accountEditar.vue'
import accountMisCursos from './components/accountMisCursos.vue'
import CursoContenido from './components/CursoContenido.vue'
import CursosCategoria from './components/CursosCategoria.vue'
import CursoVideo from './components/CursoVideo.vue'*/



const routes = [{
        path: '/',
        name: 'root',
        component: App
    },
    {
        path: '/user/logIn',
        name: "logIn",
        component: LogIn
    },
    {
        path: '/user/HomeLandingP',
        name: "HomeLandingP",
        component: HomeLandingP
    },
    {
        path: '/user/Certificate',
        name: "HomeComoCertificarte",
        component: HomeComoCertificarte
    },
    {
        path: '/user/QuienesSomos',
        name: "HomeQuienesSomos",
        component: HomeQuienesSomos
    },
    {
        path: '/user/Cursos',
        name: "HomeCursos",
        component: HomeCursos
    },
    {
        path: '/user/signUp',
        name: "signUp",
        component: SignUp
    },
    {
        path: '/user/homeUser',
        name: "home",
        component: HomeUser
    },
    {
        path: '/user/account',
        name: "account",
        component: Account
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;